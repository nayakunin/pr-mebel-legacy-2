<?php

require 'vendor/autoload.php';


if (isset ($_POST['contact'])) {

    $subject = '';
    $message = "";
    $server_par = array();
    if (isset($_POST['button']) && trim($_POST['button']) !== "") {
        $button = trim($_POST['button']);

        $message .= "<p>Кнопка: " . $button . "</p>";
        if (preg_match("#(Рассчитать стоимость)#", $button)) {
            $button = "Расчет";
        }
        if (preg_match("#(Вызвать дизайнера)#", $button)) {
            $button = "Дизайнер";
        }

        if (preg_match("#(Задать вопрос)#", $button)) {
            $button = "Вопрос";
        }
        if (preg_match("#(Оставить заявку)#", $button)) {
            $button = "Заявка";
        }


        $subject .= trim($button) . " | ";


    }
    if (isset($_POST['name']) && trim($_POST['name']) !== "") {
        $name = "<p>Имя: " . trim($_POST['name']);
        $message .= $name . "</p>";
        $server_par['name'] = trim($_POST['name']);
        $subject .= trim($_POST['name']) . " | ";
    }
    if (isset($_POST['phone']) && trim($_POST['phone']) !== "") {
        $phone = "<p>Телефон: " . trim($_POST['phone']);
        $message .= $phone . "</p>";
        $server_par['phone'] = trim($_POST['phone']);
        $subject .= trim($_POST['phone']) . " | ";
    }
    if (isset($_POST['email']) && trim($_POST['email']) !== "") {
        $email = "<p>E-mail: " . trim($_POST['email']);
        $message .= $email . "</p>";
        $server_par['email'] = trim($_POST['email']);
    }
    if (isset($_POST['message']) && trim($_POST['message']) !== "") {
        $mess = "<p>Сообщение: " . trim($_POST['message']);
        $message .= $mess . "</p>";
        $server_par['message'] = trim($_POST['message']);
    }

    if (isset($_SERVER['HTTP_REFERER']) && trim($_SERVER['HTTP_REFERER']) !== "") {
        $referer = trim($_SERVER['HTTP_REFERER']);
        $message .= "<p>URL: " . $referer . "</p>";
        $server_par['site'] = $referer;
    }

    unset($_POST['button']);
    unset($_POST['contact']);
    unset($_POST['name']);
    unset($_POST['phone']);
    unset($_POST['email']);
    unset($_POST['message']);
    if (count($_POST) > 0) {
        $ppp = "";
        $message .= "<br><p>URL параметры:</p>";
        foreach ($_POST as $key => $value) {
            $message .= "<p>" . $key . ": " . trim($value) . "</p>";
            $ppp .= $key . ': ' . trim($value) . ", ";
        }
        $ppp = rtrim($ppp, ",");
        $server_par['utm'] = $ppp;
    }

    $files = array();

    for ($ct = 0; $ct < count($_FILES['file']['tmp_name']); $ct++) {
        $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/support/upload/' . translit_without_spaces($_FILES['file']['name'][$ct]);

        $filename = $_FILES['file']['name'][$ct];
        if (move_uploaded_file($_FILES['file']['tmp_name'][$ct], $uploadfile)) {
            $files[] = str_replace($_SERVER['DOCUMENT_ROOT'], "", $uploadfile);
        } else {
            //$msg .= 'Failed to move file to ' . $uploadfile;
            unset($uploadfile);
            unset($filename);
        }
    }

    $server_par['files'] = json_encode($files, true);
    $time = time();
    $server_par['date'] = time();
    $subject .= date("d.m.Y H:i:s", $time);


    $mail2 = new PHPMailer\PHPMailer\PHPMailer;
    $mail2->CharSet = "UTF-8";
    $mail2->setFrom("info@pr-mebel.ru", $subject);
    $mail2->addAddress("nobieleadv@yandex.ru", $subject);
    $mail2->addAddress("an.nikonova2013@yandex.ru", $subject);
//$mail2->addAddress("zakaz@pr-mebel.ru",   $subject);
//$mail2->addAddress("andreygribin1993@yandex.ru",   $subject);
    $mail2->isHTML(true);
    $mail2->Subject = $subject;
    $mail2->Body = $message;


    $mail = new PHPMailer\PHPMailer\PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->SMTPAuth = true;
    $mail->isSMTP();
    $mail->SMTPSecure = 'SSL';
    $mail->SMTPDebug = 4;
    $mail->Host = 'ssl://smtp.mail.ru';
    $mail->Port = 465;
    $mail->Username = 'zakaz@pr-mebel.ru';
    $mail->Password = 'nobiele000';
    $mail->setFrom("zakaz@pr-mebel.ru", $subject);

    $mail->addAddress("zakaz@pr-mebel.ru", $subject);
//$mail->addAddress("andrey-gribin@mail.ru",   $subject);

    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $message;

    if (isset($uploadfile) && isset($filename)) {
        $mail->addAttachment($uploadfile, $filename);
        $mail2->addAttachment($uploadfile, $filename);
    }
    if ($mail->send()) {
        echo "письмо ушло";
    } else {
        echo 'Письмо не может быть отправлено. ';
        echo 'Ошибка: ' . $mail->ErrorInfo;
    }


    $mail2->send();

}


///////////////////////////////////////////////////
function translit_without_spaces($s)
{
    $s = (string)$s; // преобразуем в строковое значение
    $s = strip_tags($s); // убираем HTML-теги
    $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
    $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
    $s = trim($s); // убираем пробелы в начале и конце строки
    $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
    $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
    $s = preg_replace("/[^0-9a-z-_ \.]/i", "", $s); // очищаем строку от недопустимых символов
    $s = preg_replace("{\s+}siu", "", $s); // убираем пробелы
    return $s; // возвращаем результат
}